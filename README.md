# O

## Content

```
./O. Goga & M. Stan:
O. Goga & M. Stan - Cazul Spada 1.0 '{Politista}.docx
O. Goga & M. Stan - Intalnire cu umbrele 1.0 '{Politista}.docx
O. Goga & M. Stan - Pe frontul nevazut 1.0 '{Politista}.docx

./O. Henry:
O. Henry - Rascumpararea capeteniei pieilor rosii 1.0 '{AventuraTineret}.docx

./Oana Catina:
Oana Catina - Jocuri de copii 0.5 '{Politista}.docx

./Oana Orlea:
Oana Orlea - Cantacuzino, ia-ti boarfele si misca 1.0 '{Comunism}.docx

./Oana Pustiu:
Oana Pustiu - Despre spiritualitate cu Ovidiu Dragos Argesanu 0.99 '{Spiritualitate}.docx

./Oana Stoica Mujea:
Oana Stoica Mujea - Razboiul reginelor V1 0.99 '{Aventura}.docx

./Octav Dessila:
Octav Dessila - Iubim - V1 Inceput de viata 1.0 '{Dragoste}.docx
Octav Dessila - Iubim - V2 Sfarsit de viata 1.0 '{Dragoste}.docx
Octav Dessila - Iubim - V3 Uitam prea repede 2.0 '{Dragoste}.docx
Octav Dessila - Zvetlana 0.8 '{Dragoste}.docx

./Octavia E. Butler:
Octavia E. Butler - Sunetele vorbirii 0.99 '{SF}.docx

./Octavian Goga:
Octavian Goga - Cantece fara tara 1.0 '{Versuri}.docx
Octavian Goga - Ne cheama pamantul 1.0 '{Versuri}.docx
Octavian Goga - Noi 1.0 '{Versuri}.docx

./Octavian Paler:
Octavian Paler - Apararea lui Galilei 1.0 '{Literatura}.docx
Octavian Paler - Autoportret intr-o oglinda sparta 1.0 '{Literatura}.docx
Octavian Paler - Aventuri solitare 1.0 '{Literatura}.docx
Octavian Paler - Caminante 1.0 '{Literatura}.docx
Octavian Paler - Desertul pentru totdeauna 1.0 '{Literatura}.docx
Octavian Paler - Don Quijote in est 1.0 '{Literatura}.docx
Octavian Paler - Eul detestabil 1.0 '{Literatura}.docx
Octavian Paler - Mitologii subiective 1.0 '{Literatura}.docx
Octavian Paler - Rugati-va sa nu va creasca aripi 1.0 '{Literatura}.docx
Octavian Paler - Scrisori imaginare 1.0 '{Literatura}.docx
Octavian Paler - Un om norocos 1.0 '{Literatura}.docx
Octavian Paler - Viata ca o corida 1.0 '{Literatura}.docx
Octavian Paler - Viata pe un peron 1.0 '{Literatura}.docx

./Octavian Pantis:
Octavian Pantis - Musai List 1.0 '{DezvoltarePersonala}.docx

./Octavian Sava:
Octavian Sava - Meteoritul de aur V1 1.0 '{SF}.docx
Octavian Sava - Meteoritul de aur V2 0.99 '{SF}.docx

./Octavian Tiganus:
Octavian Tiganus - Incursiune intr-un viitor deja prezent 0.9 '{Spiritualitate}.docx

./Octavio Paz:
Octavio Paz - Dubla flacara. Dragoste si erotism 1.0 '{Filozofie}.docx

./Octav Pancu:
Octav Pancu - Fat-Frumos cand era mic 1.0 '{BasmesiPovesti}.docx

./Odile Weulersse:
Odile Weulersse - Teodora, curtezana si imparateasa 0.8 '{AventuraIstorica}.docx

./Oek de Jong:
Oek de Jong - Un cerc in iarba 0.7 '{Diverse}.docx

./Olga Caba:
Olga Caba - Nuvele fantastice 0.7 '{SF}.docx

./Olimpian Ungherea:
Olimpian Ungherea - Clubul cocosatilor - V1 Salt la trapez 1.0 '{Politista}.docx
Olimpian Ungherea - Clubul cocosatilor - V2 Zar de fuga 1.0 '{Politista}.docx
Olimpian Ungherea - Egreta brancovenilor 0.8 '{Politista}.docx
Olimpian Ungherea - Scara catre infern 0.99 '{Politista}.docx
Olimpian Ungherea - Spovedania unui spion 0.8 '{Politista}.docx

./Olimpiu Nusfelean:
Olimpiu Nusfelean - Cainii de vanatoare 0.99 '{Literatura}.docx

./Oliver Bowden:
Oliver Bowden - Assassin's Creed - V1 Renasterea 1.0 '{AventuraIstorica}.docx
Oliver Bowden - Assassin's Creed - V2 Fratia 1.0 '{AventuraIstorica}.docx
Oliver Bowden - Assassin's Creed - V3 Cruciada secreta 1.0 '{AventuraIstorica}.docx
Oliver Bowden - Assassin's Creed - V4 Revelatii 1.0 '{AventuraIstorica}.docx

./Oliver Onions:
Oliver Onions - Io 0.8 '{SF}.docx

./Oliver Sacks:
Oliver Sacks - Omul care isi confunda sotia cu o palarie 1.0 '{Literatura}.docx
Oliver Sacks - Recunostinta 1.0 '{Eseu}.docx

./Oliver Sechan & Igor B. Maslowski:
Oliver Sechan & Igor B. Maslowski - Voi, cei care n-ati fost ucisi niciodata 1.0 '{Politista}.docx

./Olivia Manning:
Olivia Manning - Orasul decazut 0.8 '{IstoricaRo}.docx
Olivia Manning - Trilogia Balcanica - V1 Marea sansa 0.7 '{IstoricaRo}.docx

./Olivier Bidchiren:
Olivier Bidchiren - Baletul astrelor 0.9 '{Diverse}.docx

./Olivier Bourdeaut:
Olivier Bourdeaut - Asteptandu-l pe Bojangles 0.99 '{Literatura}.docx

./Omar Khayyam:
Omar Khayyam - Poeme 0.8 '{Versuri}.docx

./Omraam Mikhael Aivanhov:
Omraam Mikhael Aivanhov - 2 conferinte despre Craciun 0.9 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Adevarata invatatura a lui Hristos 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Adevarul, rod al intelepciunii si al iubirii 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - A doua nastere 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Alchimia spirituala - Cautarea perfectiunii 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - A primi si a darui 0.9 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Armonie si sanatate 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Aura, pielea noastra spirituala 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Aura plexul solar, centrul Hara Kundalini si Chakrele 0.9 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Balanta cosmica - Numarul 2 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Calea tacerii 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Cartea magiei divine 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Casatoria simbol universal 0.99 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Cateva aspecte simbolice ale Craciunului 0.99 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Ce este un Fiu al Domnului 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Ce este un maestru spiritual 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Centri si corpuri subtile 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Chakrele si forta Kundalini 0.9 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Colectia Fascicule 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Craciunul 0.99 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Craciunul si Pastele in traditia initiatica 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Creatie artistica si creatie spirituala 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Credinta care muta muntii 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Cufundarea in tacere 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - De la om la Dumnezeu 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Despre deschiderea simturilor interioare 0.99 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Despre dezbracare 0.99 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Despre ingerii celor 4 elemente 0.99 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Destainuirile focului si ale apei 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Determinism si nedeterminism 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Egregorul porumbelului pacii 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Filozofia universalului 0.99 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Forta sexuala sau dragonul inaripat 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Galvanoplastia spirituala 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Grauntele fericirii 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Hrana, o scrisoare de iubire de la creator 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Hrani yoga 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Inaltul ideal 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Incercarile neasteptate ale vietii 0.99 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Incursiune in invizibil 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - In duh si adevar 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - In pragul cetatii sfinte 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Iubirea mai mare decat credinta 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - La izvorul cristalin al bucuriei 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Legile moralei cosmice 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Libertatea, victorie a spiritului 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Limbajul figurilor geometrice 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Lumina, spirit viu 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Meditatia 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Misterele lui Iesod 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Moartea si viata de dincolo 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Muzica si cantecul in viata spirituala 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Natura umana si natura divina 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Noul pamant sau reguli de aur 0.9 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - O educatie incepe inainte de nastere 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - O filozofie a universalului 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Omul spre victoria destinului sau 0.99 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Pacea, o stare de constiinta superioara 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Pasiti cat aveti lumina 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Pentru a deveni o carte vie 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Piatra filosofala 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Pomul cunoasterii binelui si al raului 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Practicile focului si apei 0.9 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Pretul libertatii 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Puterea magica a increderii 0.99 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Puterile gandului 0.99 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Puterile gandului 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Puterile vietii 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Rasul inteleptului 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Reguli de aur pentru fiecare zi 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Respiratia 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Sarbatoarea de Paste, 'Eu sunt invierea si viata' 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Secretele cartii naturii 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Semintele fericirii 0.99 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Sim 0.2 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Spre o civilizatie solara 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Sufletul, instrument al spiritului 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Un nou inteles al evangheliilor 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Un viitor pentru tineret 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Viata, bunul cel mai de pret 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Viata psihica - Elemente si structuri 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Yoga nutritiei 1.0 '{Spiritualitate}.docx
Omraam Mikhael Aivanhov - Zodiacul, cheia omului si universului 1.0 '{Spiritualitate}.docx

./Oren Harari:
Oren Harari - Secretele lui Colin Powell 1.0 '{Istorie}.docx

./Orhan Pamuk:
Orhan Pamuk - Cartea neagra 1.0 '{Literatura}.docx
Orhan Pamuk - Casa tacerii 1.0 '{Literatura}.docx
Orhan Pamuk - Cevdet Bey si fiii sai 0.99 '{Literatura}.docx
Orhan Pamuk - Fortareata alba 1.0 '{Literatura}.docx
Orhan Pamuk - Istambul 0.99 '{Literatura}.docx
Orhan Pamuk - Ma numesc Rosu 1.0 '{Literatura}.docx
Orhan Pamuk - Muzeul inocentei 1.2 '{Literatura}.docx
Orhan Pamuk - Romancierul naiv si sentimental 1.0 '{Literatura}.docx
Orhan Pamuk - Viata cea noua 1.0 '{Literatura}.docx
Orhan Pamuk - Zapada 1.0 '{Literatura}.docx

./Orson Scott Card:
Orson Scott Card - Ender - V1 Jocul lui Ender 2.1 '{SF}.docx
Orson Scott Card - Ender - V2 Vorbitor in numele mortilor 1.0 '{SF}.docx
Orson Scott Card - Ender - V3 Xenocide 1.0 '{SF}.docx
Orson Scott Card - Ender - V4 Copiii mintii 1.1 '{SF}.docx
Orson Scott Card - Ender - V5 Umbra lui Ender 0.99 '{SF}.docx
Orson Scott Card - Ender - V6 Umbra hegemonului 0.99 '{SF}.docx
Orson Scott Card - Ender - V7 Umbra marionetelor 0.9 '{SF}.docx
Orson Scott Card - Ender - V8 Umbra uriasului 0.9 '{SF}.docx
Orson Scott Card - Intoarcerea Acasa - V1 Amintirea pamantului 1.9 '{SF}.docx
Orson Scott Card - Intoarcerea Acasa - V2 Chemarea Pamantului 1.0 '{SF}.docx
Orson Scott Card - Intoarcerea Acasa - V3 Navele Pamantului 1.0 '{SF}.docx
Orson Scott Card - Intoarcerea Acasa - V4 din nou pe Pamant 1.0 '{SF}.docx
Orson Scott Card - Intoarcerea Acasa - V5 Nascuti pe Pamant 1.0 '{SF}.docx
Orson Scott Card - Wyrm 2.0 '{SF}.docx

./Os. Kuhlen:
Os. Kuhlen - Sistemul ocult de dominare a lumii. Istoria secreta a umanitatii 2.0 '{MistersiStiinta}.docx

./Oscar Cullmann:
Oscar Cullmann - Nemurirea sufletului sau invierea trupului 0.8 '{Religie}.docx

./Oscar Wilde:
Oscar Wilde - Portretul lui Dorian Gray 0.99 '{ClasicSt}.docx

./Osho Rajneesh:
Osho Rajneesh - Amorul Tantric 1.0 '{Spiritualitate}.docx
Osho Rajneesh - Biografie 1.0 '{Spiritualitate}.docx
Osho Rajneesh - Bucuria 1.0 '{Spiritualitate}.docx
Osho Rajneesh - Buddha Spune (frag.) 1.0 '{Spiritualitate}.docx
Osho Rajneesh - Calea ZEN 2.0 '{Spiritualitate}.docx
Osho Rajneesh - Cand iubesti 1.0 '{Spiritualitate}.docx
Osho Rajneesh - Cartea despre barbati 1.0 '{Spiritualitate}.docx
Osho Rajneesh - Cartea despre copii 1.0 '{Spiritualitate}.docx
Osho Rajneesh - Cartea despre femei 1.0 '{Spiritualitate}.docx
Osho Rajneesh - Cartea despre sex 1.0 '{Spiritualitate}.docx
Osho Rajneesh - Cartea despre TAO 1.0 '{Spiritualitate}.docx
Osho Rajneesh - Cartea secreta esentiala a caii tantrice V1 1.0 '{Spiritualitate}.docx
Osho Rajneesh - Cartea secreta esentiala a caii tantrice V2 2.0 '{Spiritualitate}.docx
Osho Rajneesh - Cartea secreta esentiala a caii tantrice V3 2.0 '{Spiritualitate}.docx
Osho Rajneesh - Cartea secreta esentiala a caii tantrice V4 2.0 '{Spiritualitate}.docx
Osho Rajneesh - Carti scrise dupa discursurile maestrului indian Osho Rajneesh (frag.) 1.0 '{Spiritualitate}.docx
Osho Rajneesh - Catre Trezire V1 1.0 '{Spiritualitate}.docx
Osho Rajneesh - Constientizarea (frag.) 1.0 '{Spiritualitate}.docx
Osho Rajneesh - Cum pot iubi mai mult (frag.) 1.0 '{Spiritualitate}.docx
Osho Rajneesh - Curajul 1.0 '{Spiritualitate}.docx
Osho Rajneesh - Destin, libertate si suflet 2.0 '{Spiritualitate}.docx
Osho Rajneesh - Dhammapada. Calea legii divine relevata de Buddha V1 2.0 '{Spiritualitate}.docx
Osho Rajneesh - Dincolo de mister 1.0 '{Spiritualitate}.docx
Osho Rajneesh - Emotiile 1.0 '{Spiritualitate}.docx
Osho Rajneesh - Eu insa va zic voua V1 1.0 '{Spiritualitate}.docx
Osho Rajneesh - Farmacie Pentru Suflet 1.0 '{Spiritualitate}.docx
Osho Rajneesh - Frica 1.0 '{Spiritualitate}.docx
Osho Rajneesh - Hari Om Tat Sat (frag.) 1.0 '{Spiritualitate}.docx
Osho Rajneesh - Intimitatea. Increderea in sine si in celalalt 1.0 '{Spiritualitate}.docx
Osho Rajneesh - Iubire libertate si solitudine 1.0 '{Spiritualitate}.docx
Osho Rajneesh - Meditatia - Calea perfecta 2.0 '{Spiritualitate}.docx
Osho Rajneesh - Meditatia - Cunoasterea de sine 1.0 '{Spiritualitate}.docx
Osho Rajneesh - Meditatia - Prima si ultima libertate 1.0 '{Spiritualitate}.docx
Osho Rajneesh - Meditatia Vipasana (frag.) 1.0 '{Spiritualitate}.docx
Osho Rajneesh - Nu te lasa fraierit (frag.) 1.0 '{Spiritualitate}.docx
Osho Rajneesh - Parabola despre dragoste si ego 1.0 '{Spiritualitate}.docx
Osho Rajneesh - Patanjali - Yoga Sutra - Stiinta sufletului V5 2.0 '{Spiritualitate}.docx
Osho Rajneesh - Revolutia interioara 1.0 '{Spiritualitate}.docx
Osho Rajneesh - Samanta de mustar (frag. - V1 cap.14) 1.0 '{Spiritualitate}.docx
Osho Rajneesh - Samanta de mustar (frag. - V2 cap.18) 1.0 '{Spiritualitate}.docx
Osho Rajneesh - Socrate poisoned again after 25 centuries (frag. - cap.10) 1.0 '{Spiritualitate}.docx
Osho Rajneesh - Spiritualitatea tantrica V1 1.0 '{Spiritualitate}.docx
Osho Rajneesh - Spiritualitatea tantrica V2 1.0 '{Spiritualitate}.docx
Osho Rajneesh - Starea de Iluminare 1.0 '{Spiritualitate}.docx
Osho Rajneesh - Tao. The pathless path (frag. - V2 cap.4) 1.0 '{Spiritualitate}.docx
Osho Rajneesh - Viata este aici si acum 1.0 '{Spiritualitate}.docx

./Otilia Hedesan:
Otilia Hedesan - Pentru o mitologie difuza 0.7 '{Diverse}.docx

./Ottessa Moshfegh:
Ottessa Moshfegh - Eileen 1.0 '{Literatura}.docx

./Otto Ludwig:
Otto Ludwig - Intre cer si pamant 1.0 '{Literatura}.docx

./Ovidiu Bufnila:
Ovidiu Bufnila - Big bang boo 0.99 '{SF}.docx
Ovidiu Bufnila - Cadavre de lux 1.0 '{SF}.docx
Ovidiu Bufnila - Calatoriile mele 0.99 '{Necenzurat}.docx
Ovidiu Bufnila - Campurile magnetice ale lui Belizarie 0.2 '{SF}.docx
Ovidiu Bufnila - Campuri magnetice 0.99 '{SF}.docx
Ovidiu Bufnila - Codul Ican 0.9 '{SF}.docx
Ovidiu Bufnila - Compresor 0.2 '{SF}.docx
Ovidiu Bufnila - Corabia nebunilor 0.99 '{SF}.docx
Ovidiu Bufnila - Corabiile lungi 1.0 '{SF}.docx
Ovidiu Bufnila - Cruciada bucatarilor 1.0 '{SF}.docx
Ovidiu Bufnila - Cruciada lui Moreaugarin 1.0 '{SF}.docx
Ovidiu Bufnila - Experienta insoteste sensul 0.2 '{SF}.docx
Ovidiu Bufnila - Falci insangerate 0.2 '{SF}.docx
Ovidiu Bufnila - Fanfara municipala 0.6 '{SF}.docx
Ovidiu Bufnila - Flida Flado 0.2 '{SF}.docx
Ovidiu Bufnila - Gena mortii 0.2 '{SF}.docx
Ovidiu Bufnila - Inelul magic 0.99 '{SF}.docx
Ovidiu Bufnila - Jazzonia - Cartea fictiunilor 0.7 '{SF}.docx
Ovidiu Bufnila - Jazzonia - Cartea navigatorilor 0.99 '{SF}.docx
Ovidiu Bufnila - Luna pe din doua 0.99 '{SF}.docx
Ovidiu Bufnila - Mamica si el diabolo 0.99 '{SF}.docx
Ovidiu Bufnila - Marele absent 0.99 '{SF}.docx
Ovidiu Bufnila - Meduza 0.6 '{SF}.docx
Ovidiu Bufnila - Necunoscutul intrigant 0.9 '{SF}.docx
Ovidiu Bufnila - Sfarsitul primaverii in Puerto Pico 1.0 '{SF}.docx
Ovidiu Bufnila - Submarinul rosu 0.9 '{SF}.docx
Ovidiu Bufnila - Universul lent 0.99 '{SF}.docx

./Ovidiu Constantinescu:
Ovidiu Constantinescu - Menestrelii regelui Ludovic 1.0 '{Dragoste}.docx

./Ovidiu Dragos Argesanu:
Ovidiu Dragos Argesanu - 2006 Atacul Psi 0.99 '{Spiritualitate}.docx
Ovidiu Dragos Argesanu - 2007 Cele 7 peceti 1.0 '{Spiritualitate}.docx
Ovidiu Dragos Argesanu - 2011 Arta razboiului Psi. Protectia 0.9 '{Spiritualitate}.docx
Ovidiu Dragos Argesanu - 2011 Arta razboiului Psi 2.0 '{Spiritualitate}.docx
Ovidiu Dragos Argesanu - 2011 Jumatatea mea femeia 1.0 '{Spiritualitate}.docx
Ovidiu Dragos Argesanu - 2012 Arta terapiei Psi 1.0 '{Spiritualitate}.docx
Ovidiu Dragos Argesanu - 2012 Kombat Ki 1.0 '{Spiritualitate}.docx
Ovidiu Dragos Argesanu - 2012 Reiki intre mit si realitate 1.0 '{Spiritualitate}.docx
Ovidiu Dragos Argesanu - 2013 Karma si dreptul divin. Trezirea spirituala. de la sex la indumnezeire 1.0 '{Spiritualitate}.docx
Ovidiu Dragos Argesanu - A treia venire 1.0 '{Spiritualitate}.docx
Ovidiu Dragos Argesanu - Curs Gendai grad 1-2 0.9 '{Spiritualitate}.docx
Ovidiu Dragos Argesanu - Curs Veritas gradul 1 1.0 '{Spiritualitate}.docx

./Ovidiu Eftimie:
Ovidiu Eftimie - Arhangelul Raul 1.0 '{SF}.docx

./Ovidiu Harbada:
Ovidiu Harbada - De vorba cu Valeriu Popa despre sanatate si viata 1.0 '{Sanatate}.docx
Ovidiu Harbada - Miracolul din fiecare 1.0 '{Sanatate}.docx
Ovidiu Harbada - Nasterea si moartea 1.0 '{Sanatate}.docx
Ovidiu Harbada - Valeriu Popa - Indrumator al cunoasterii prin stiinta si credinta 1.0 '{Sanatate}.docx

./Ovidiu Petcu:
Ovidiu Petcu - Cimpanzeii albastri 0.99 '{SF}.docx

./Ovidiu Radulescu:
Ovidiu Radulescu - Mancand panem privind circenses 0.8 '{Editorial}.docx
Ovidiu Radulescu - Paradis cu inlocuitori 0.8 '{Diverse}.docx

./Ovidiu Raureanu:
Ovidiu Raureanu - Aventurile lui Serban Andronic 0.7 '{SF}.docx
Ovidiu Raureanu - La steaua de mare 0.9 '{SF}.docx

./Ovidius Naso:
Ovidius Naso - Metamorfoze 0.7 '{ClasicSt}.docx

./Ovidiu Surianu:
Ovidiu Surianu - Intalnire cu Hebe 2.0 '{SF}.docx
Ovidiu Surianu - Padurea scorpionilor 0.99 '{SF}.docx

./Ovidiu Verdes:
Ovidiu Verdes - Muzici si faze 0.6 '{Diverse}.docx

./Ovidiu Vitan:
Ovidiu Vitan - Omul care a ucis pentru mandria ranita a lumii 0.9 '{SF}.docx

./Ovidiu Zotta:
Ovidiu Zotta - Operatiunea Hercule 1.0 '{AventuraTineret}.docx
Ovidiu Zotta - Spadasinul de serviciu 1.0 '{AventuraTineret}.docx

./Ovid S. Crohmalniceanu:
Ovid S. Crohmalniceanu - Alte istorii insolite 0.9 '{Diverse}.docx
Ovid S. Crohmalniceanu - Istorii insolite 1.0 '{Diverse}.docx

./Oyinkan Braithwaite:
Oyinkan Braithwaite - Sora mea, ucigasa in serie 1.0 '{Literatura}.docx
```

